﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface ICanDetect
{
    Collider DetectArea(Transform startDetectPoint, float ViewRadius, float ViewAngle, Collider[] detectedColliders, int numberOfCollidersDetected, LayerMask interestedLayerMask);
    void ShowDetect(Transform startDetectPoint, float ViewRadius, float ViewAngle, RectTransform coneVision, Camera minimapCamera, RectTransform minimap);
}
    

