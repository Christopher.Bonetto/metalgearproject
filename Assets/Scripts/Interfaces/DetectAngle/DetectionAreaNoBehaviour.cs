﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class DetectionAreaNoBehaviour : MonoBehaviour, ICanDetect
{
    /*summary
     * Method used to return a Collider.
     * it check if there is the interested layer intro a zone.
     * if it found something return how much colliders it has founded.
     * check them are into the customizable cone.
     * check if there is an obstacles between the StartDetectPoint to the Collider's point. It does this for each collider it has founded before.
     * after that check if the interested object has a RigidBody component.
     * if all of them return a value this method will return a collider. That will let to know his position or take his components.
     */
    public Collider DetectArea(Transform StartDetectPoint, float ViewRadius, float ViewAngle, Collider[] DetectedColliders, int NumberOfCollidersDetected, LayerMask InterestedLayerMask)
    {
        NumberOfCollidersDetected = Physics.OverlapSphereNonAlloc(StartDetectPoint.transform.position, ViewRadius, DetectedColliders, InterestedLayerMask);
                
        for (int i = 0; i < NumberOfCollidersDetected; i++)
        {
            if (DetectedColliders[i])
            {
                float angle = Vector3.Angle(StartDetectPoint.transform.forward, DetectedColliders[i].transform.position - StartDetectPoint.transform.position);

                RaycastHit hit;

                if (Mathf.Abs(angle) < ViewAngle / 2 && Physics.Raycast(StartDetectPoint.transform.position, DetectedColliders[i].transform.position - StartDetectPoint.transform.position, out hit, ViewRadius, InterestedLayerMask))
                {

                    if (hit.collider.GetComponent<Rigidbody>())
                    {
                        Debug.DrawLine(StartDetectPoint.transform.position, DetectedColliders[i].transform.position, Color.red);
                        
                        return DetectedColliders[i];
                    }
                }

            }
        }
        return null;
    }

    /*summary
     * Method used to show the cone explained above.
     * After all translate the object's world position in a camera position.
     * Delimits the limits within the visual range of the camera.
     * After set the image'position to the startDetectPoint converted position.
     * To resize the Radius will rezize the x's local scale of the image.
     * To resize the angle is different. The image have two sliders (children) that them will change them fill value at the same time. One in clockwise and one not.
     */
    public void ShowDetect(Transform startDetectPoint, float ViewRadius, float ViewAngle, RectTransform coneVision, Camera minimapCamera, RectTransform minimap)
    {
        RectTransform minimapSizeRect = minimap.GetComponent<RectTransform>();
        
        Vector2 viewportPosition = minimapCamera.WorldToViewportPoint(startDetectPoint.transform.position);
        Vector2 objectInScreenPosition = new Vector2(((viewportPosition.x * minimapSizeRect.sizeDelta.x) - (minimapSizeRect.sizeDelta.x * 0.5f)),((viewportPosition.y * minimapSizeRect.sizeDelta.y) - (minimapSizeRect.sizeDelta.y * 0.5f)));
        
        coneVision.anchoredPosition = objectInScreenPosition;
        
        coneVision.localEulerAngles = Vector3.forward *(startDetectPoint.eulerAngles.y - 90f) * -1f;
        
        coneVision.GetComponentInChildren<RectTransform>().localScale = new Vector3(ViewRadius / 60, coneVision.localScale.y, coneVision.localScale.z);
                
        float newAngle = ViewAngle / 720;
        
        Image[] allChildren = coneVision.GetComponentsInChildren<Image>();
        foreach (Image child in allChildren)
        {
            child.fillAmount = newAngle;
        }
    }
}
