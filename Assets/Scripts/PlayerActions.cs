﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class PlayerActions : MonoBehaviour
{
    private Animator chris_playerAnimator;
    private float chris_startAnimationsSpeed;
    private float chris_animationTimer;
    [SerializeField]
    private int MG_AnimationChangeIdleDelay;

    private NavMeshAgent chris_agent;
    private Vector3 chris_movement;

    #region Detection Area

    public ICanDetect MG_playerDetectRef { get; private set; }

    [Range(0f,130f),SerializeField]
    private float chris_playerViewAngle;

    public float CurrentAngle
    {
        get
        {
            return Mathf.Clamp(chris_playerViewAngle, 0, 130f);
        }
        private set
        {
            chris_playerViewAngle = value;
        }
    }

    [Range(0f,2f),SerializeField]
    private float chris_playerViewRadius;

    public float CurrentRadius
    {
        get
        {
            return Mathf.Clamp(chris_playerViewAngle, 0, 2f);
        }
        private set
        {
            chris_playerViewAngle = value;
        }
    }

    [SerializeField]
    private LayerMask chris_detectionMask;
    
    private Collider[] chris_playerDetectedColliders = new Collider[1];
    private int chris_numberOfColliderDetected;

    private Vector3 chris_enemyPosition;
    private EnemySoldier chris_enemyRef;
    private Collider enemyColliderRef;
    #endregion

    private void Awake()
    {
        chris_agent = gameObject.GetComponent<NavMeshAgent>();
        chris_playerAnimator = gameObject.GetComponentInChildren<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        chris_agent.updateRotation = false;

        MG_playerDetectRef = new DetectionAreaNoBehaviour();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        AnimationsManager();
        Attack();
    }
    
    //Move the player taken GetAxis values
    private void Move()
    {
        if (GameManager.MG_Instance.MG_CurrentGameStates == GameStates.Running)
        {
            chris_movement = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized;
            chris_agent.Move(chris_movement * chris_agent.speed * Time.deltaTime);

            if(chris_movement != Vector3.zero)
            {
                transform.forward = chris_movement;
            }
            
        }
    }

    //Called to stop the player.
    public void StopPlayer()
    {
        chris_movement = Vector3.zero;
    }

    private void Attack()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (MG_playerDetectRef.DetectArea(gameObject.transform, chris_playerViewRadius, chris_playerViewAngle, chris_playerDetectedColliders, chris_numberOfColliderDetected, chris_detectionMask) != null)
            {
                PlayTriggerAnimation("Punch");
                chris_enemyRef = MG_playerDetectRef.DetectArea(gameObject.transform, chris_playerViewRadius, chris_playerViewAngle, chris_playerDetectedColliders, chris_numberOfColliderDetected, chris_detectionMask).GetComponent<EnemySoldier>();
                chris_enemyRef.ChangeSoldierState(SoldierStates.Stunned);
            }
        }
    }
    
    private void AnimationsManager()
    {
        AnimationsSpeed();
        RandomIdle();      
    }

    private void AnimationsSpeed()
    {
        if (chris_movement != Vector3.zero)
        {
            chris_playerAnimator.SetFloat("speed", chris_agent.speed);
        }
        else
        {
            chris_playerAnimator.SetFloat("speed", 0);
        }
    }

    //Method used when the player's movement is 0.
    //If the player play the standard idle animation start a timer with lenght like the idle animation.
    //When the timer reach the animation's duration moltiplied for each time i want the player will reproduce the standard idle,
    //this method will play a random idle animation.
    private void RandomIdle()
    {
        if (chris_playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("NormalIdle"))
        {
            chris_animationTimer += Time.deltaTime;

            if (chris_animationTimer >= chris_playerAnimator.GetCurrentAnimatorStateInfo(0).length * MG_AnimationChangeIdleDelay)
            {
                int randomIdle = Random.Range(0, 2);

                if (randomIdle == 0)
                {
                    PlayTriggerAnimation("Idle00");
                    chris_animationTimer = 0;
                }
                else if (randomIdle == 1)
                {
                    PlayTriggerAnimation("Idle01");
                    chris_animationTimer = 0;
                }
            }
        }
    }
    
    public void PlayTriggerAnimation(string NameAnimations)
    {
        chris_playerAnimator.SetTrigger(NameAnimations);
    }



    #region DetectionGizmo
    //Gizmo too see the OverlapSphere

#if UNITY_EDITOR

    public void OnDrawGizmosSelected()
    {
        EditorGizmo(transform);
    }

    public void EditorGizmo(Transform transform)
    {
        ViewGizmo(Color.green, chris_playerViewAngle, chris_playerViewRadius);
    }

    private void ViewGizmo(Color color, float angle, float radius)
    {
        Color c = color;
        UnityEditor.Handles.color = c;

        Vector3 rotatedForward = Quaternion.Euler(0, -angle * 0.5f, 0) * transform.forward;

        UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.up, rotatedForward, angle, radius);
    }
#endif

    #endregion
}
