﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VictoryItem : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(2f, 2f, 0));
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerActions pa = other.GetComponent<PlayerActions>();

        if (pa)
        {
            GameManager.MG_Instance.SetManagerState(GameStates.Victory);
            pa.StopPlayer();
            gameObject.GetComponent<Collider>().enabled = false;
        }
    }
}
