﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager MG_Instance;
    
    [SerializeField]
    private Image MG_FadeImage;
    private float chris_fadeStartTime = 0;
    [SerializeField]
    private Color MG_BlackColor;
    [SerializeField]
    private Color MG_TrasparentColor;

    [HideInInspector] public bool changeColor;
    
    [SerializeField]
    private Text MG_LevelTimeShowText;

    [SerializeField]
    private GameObject timerPanel, pausePanel, losePanel, victoryPanel;
    
    
    private void Awake()
    {
        MG_Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        StartGame();
        
    }

    // Update is called once per frame
    void Update()
    {
        MG_LevelTimeShowText.text = GameManager.MG_Instance.CurrentTime.ToString("F");
    }

    #region ShowPanels
    // According to the value of the bool will activate the interested gameobject and will deactivate the rest.

    public void ShowTimer(bool value)
    {
        if (timerPanel.gameObject.activeSelf != value)
        {
            timerPanel.gameObject.SetActive(value);
            pausePanel.gameObject.SetActive(!value);
            losePanel.gameObject.SetActive(!value);
            victoryPanel.gameObject.SetActive(!value);
            MG_FadeImage.gameObject.SetActive(!value);
        }
    }

    public void ShowPause(bool value)
    {
        if (pausePanel.gameObject.activeSelf != value)
        {
            pausePanel.gameObject.SetActive(value);
            timerPanel.gameObject.SetActive(!value);
            losePanel.gameObject.SetActive(!value);
            victoryPanel.gameObject.SetActive(!value);
        }
    }

    public void ShowLose(bool value)
    {
        if (losePanel.gameObject.activeSelf != value)
        {
            timerPanel.gameObject.SetActive(!value);
            losePanel.gameObject.SetActive(value);
            victoryPanel.gameObject.SetActive(!value);
            pausePanel.gameObject.SetActive(!value);
           
        }
    }

    public void ShowWin(bool value)
    {
        if (losePanel.gameObject.activeSelf != value)
        {
            timerPanel.gameObject.SetActive(!value);
            losePanel.gameObject.SetActive(!value);
            victoryPanel.gameObject.SetActive(value);
            pausePanel.gameObject.SetActive(!value);

        }
    }
    #endregion

    public void StartGame()
    {
        timerPanel.gameObject.SetActive(false);
        losePanel.gameObject.SetActive(false);
        victoryPanel.gameObject.SetActive(false);
        pausePanel.gameObject.SetActive(false);
    }

    #region Fade

    public void EnableImage(bool newValue)
    {
        MG_FadeImage.gameObject.SetActive(newValue);
    }

    //change the image's color according to the timer. This method will activate the fadeImage and set the bool changeColor true. When the bool is true will start a timer.
    //While the timer's value increase the image's color change from black to trasparent.
    //the the timer reach the max value set the bool false and deactivate the fade image.
    public bool FadeFromBlack(float maxTimeToFade = 1)
    {
        if (changeColor)
        {
            EnableImage(true);

            if (chris_fadeStartTime < maxTimeToFade)
            {
                chris_fadeStartTime += Time.deltaTime * 0.2f;
                MG_FadeImage.color = Color.Lerp(MG_BlackColor, MG_TrasparentColor, chris_fadeStartTime);
                return true;
            }
            if (chris_fadeStartTime >= maxTimeToFade)
            {
                changeColor = false;
                EnableImage(false);
            }

        }
        return false;
        
    }
    #endregion
}
