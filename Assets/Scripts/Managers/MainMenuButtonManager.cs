﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtonManager : MonoBehaviour
{
    public static MainMenuButtonManager MG_Instance;

    public GameObject MG_MainPanel;
    public GameObject[] MG_Panels;
    
    private AudioSource chris_audioSource;
    public AudioClip MG_ClickSound;

    private void Awake()
    {
        chris_audioSource = gameObject.GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    void Start()
    {
        MG_Instance = this;
        StartScene();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //Disactivates all UI gameobjects except the first and the second.
    private void StartScene()
    {
        for (int i = 0; i < MG_Panels.Length; i++)
        {
            if(i <= 1)
            {
                MG_Panels[i].SetActive(true);
                
            }
            else
            {
                MG_Panels[i].SetActive(false);
            }
        }

    }

    //When this button will be clicked play a sound and take the index of the object. Will deactivate the one pressed and will activate the panels with the interested index chosen in ispector.
    public void ClickedButton(int indexOfEnableItem)
    {
        for (int i = 1; i < MG_Panels.Length; i++)
        {
            if(i == indexOfEnableItem)
            {
                MG_Panels[i].SetActive(true);
                chris_audioSource.PlayOneShot(MG_ClickSound, 0.5f);
            }
            else
            {
                MG_Panels[i].SetActive(false);
            }
        }
    }

    //Load a scene according to the inspector's index chosen.
    public void ChooseSceneToLoad(int indexScene)
    {
        chris_audioSource.PlayOneShot(MG_ClickSound, 0.5f);
        SceneManager.LoadScene(indexScene);
    }

}
