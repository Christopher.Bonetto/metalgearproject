﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameStates
{
    Start,
    Running,
    Pause,
    Lose,
    Victory
}

public class GameManager : MonoBehaviour
{
    public static GameManager MG_Instance;

    public GameStates MG_CurrentGameStates {get; private set;}

    #region Level Time Variables

    [SerializeField]
    private float chris_maxTimeForLevel;

    public float CurrentTime
    {
        get
        {
            return Mathf.Clamp(chris_maxTimeForLevel, 0, chris_maxTimeForLevel);
        }
        private set
        {
            chris_maxTimeForLevel = value;
        }
    }

    private float chris_timerToLoseWin;

    #endregion

    private int chris_indexCurrentScene;
    
    [SerializeField]
    private float chris_destinationTime;
    private float chris_timer;

    #region Start Time Variables

    [Range(0,1),SerializeField]
    private float chris_timeToStart;

    public float MG_TimeToStartGame
    {
        get
        {
            return Mathf.Clamp(chris_timeToStart, 0f, 1f);
        }
        private set
        {
            chris_timeToStart = value;
        }
    }

    #endregion

    public PlayerActions MG_playerReference;
    public CameraActions MG_mainCamera;

    private void Awake()
    {
        MG_Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        chris_indexCurrentScene = SceneManager.GetActiveScene().buildIndex;
        SetManagerState(GameStates.Start);
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        GameStatesManager();
    }

    // Check all GameManager's State.
    private void GameStatesManager()
    {
        switch (MG_CurrentGameStates)
        {
            case GameStates.Start:
                StartTheLevel();
                break;

            case GameStates.Running:
                GameTime();
                PauseActions(KeyCode.Escape);
                break;

            case GameStates.Pause:
                PauseActions(KeyCode.Escape);
                break;

            case GameStates.Lose:
                MG_mainCamera.ChangeCameraState(CameraStates.Zoom);
                WinLoseActions(chris_destinationTime);
                break;

            case GameStates.Victory:
                MG_mainCamera.ChangeCameraState(CameraStates.Zoom);
                WinLoseActions(chris_destinationTime);
                break;
        }
    }

    //Used to change state.
    public void SetManagerState(GameStates newGameState)
    {
        MG_CurrentGameStates = newGameState;
    }

    //At the start set ui and camera. UI will have to do a fade, and the camera will move just when the fade will be complete..
    private void StartTheLevel()
    {
        UIManager.MG_Instance.changeColor = true;

        if (UIManager.MG_Instance.FadeFromBlack(MG_TimeToStartGame))
        {
            
            MG_mainCamera.ChangeCanMove(false); 
        }
        else
        {
            MG_mainCamera.ChangeCanMove(true);
        }
    }

    //When the game is Running the level's time decreases. If reach 0 the player will lose.
    private void GameTime()
    {
        if(chris_maxTimeForLevel > 0)
        {
            chris_maxTimeForLevel -= Time.deltaTime;
        }
        else
        {
            SetManagerState(GameStates.Lose);
        }
    }

    //Set key to change TimeScale value (0 or 1)
    private void PauseActions(KeyCode keyCode)
    {
        if (Input.GetKeyDown(keyCode))
        {
            GamePause();
        }
        
    }

    
    /*Method to determine if the player has won or lost.
     * First of all stop the player.
     * A timer values will increase.
     * And when the timer reach the maxValue will show Victory/Lose panels.
     */
    private void WinLoseActions(float destinationTime)
    {
        MG_playerReference.StopPlayer();

        chris_timerToLoseWin += Time.deltaTime;

        if (MG_CurrentGameStates == GameStates.Lose)
        {
            MG_playerReference.PlayTriggerAnimation("Defeat");

            if (chris_timerToLoseWin >= destinationTime)
            {
                UIManager.MG_Instance.ShowLose(true);
            }
        }
        else if (MG_CurrentGameStates == GameStates.Victory)
        {
            MG_playerReference.PlayTriggerAnimation("Victory");

            if (chris_timerToLoseWin >= destinationTime)
            {
                UIManager.MG_Instance.ShowWin(true);
            }
        }
    }

    #region Bottons

    //Check the game's timescale. If it is 0 return 1 and viceversa.
    //Enable and disable pause panel and timer panel.
    public void GamePause()
    {
        Time.timeScale = Time.timeScale == 0 ? 1 : 0;

        if (Time.timeScale == 0)
        {
            SetManagerState(GameStates.Pause);
            UIManager.MG_Instance.ShowPause(true);
        }
        else if (Time.timeScale == 1)
        {
            SetManagerState(GameStates.Running);
            UIManager.MG_Instance.ShowTimer(true);
        }
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ResturnToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    //At the start of the game the GM take the scene's index like int.
    // When this method is called check if the index the last in the list's scenes. If is the last return to the main scene.
    public void ChangeToNextScene()
    {
        
        if (chris_indexCurrentScene < SceneManager.sceneCountInBuildSettings - 1)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }
    #endregion
}
