﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum SoldierStates
{
    Stunned,
    Normal,
    Patrolling,
    Allert,
    LoseTarget
}

public class EnemySoldier : Enemies
{
    #region MovementVariables
    public NavMeshAgent chris_enemyAgent { get; private set; }

    private Vector3 chris_startedPosition;
    private Quaternion chris_startedRotation;
    private bool chris_returnToPosition;

    private float chris_startedSpeed;
    public float MG_AllertSpeed;
    #endregion

    public SoldierStates chris_soldierCurrentState { get; private set; }
    public SoldierStates chris_startedState { get; private set; }

    [SerializeField]
    private GameObject chris_patrolPointsBox;
    public Transform[] MG_PatrolPoints;
    private int chris_patrolCount;

    #region Animator Variables

    private Animator chris_enemyAnimatorController;
    private float chris_animationsSpeed;
    private float chris_animationTimer;
    [SerializeField]
    private float chris_animationChangeIdleDelay;

    #endregion

    #region LoseTarget Variables

    private float chris_stunnedTimer;
    private float chris_timer;
    [SerializeField]
    private float chris_rotationSpeed;
    [SerializeField]
    private float chris_DelayChangeRotation;
    private bool chris_changeRotationDirection;
    private int chris_numberOfRotation;
    [SerializeField]
    private int chris_desideredRotations;

    private Quaternion chris_lookRotationForward;

    [SerializeField]
    private float chris_distanceToLoseGame;

    #endregion

    public override void Awake()
    {
        base.Awake();
        chris_enemyAgent = gameObject.GetComponent<NavMeshAgent>();
        chris_enemyAnimatorController = gameObject.GetComponentInChildren<Animator>();
    }

    public override void Start()
    {
        base.Start();
        
        SetStartingState();
        SaveStartingVariables();
    }

    public override void Update()
    {
        base.Update();

        Allerted();
        CheckIfCanMove();
        AnimationsManager();

        switch (chris_soldierCurrentState)
        {
            case SoldierStates.Normal:
                CheckPosition();
                break;

            case SoldierStates.Patrolling:
                Patrolling();
                break;

            case SoldierStates.Allert:
                AllertState();
                break;

            case SoldierStates.LoseTarget:
                LoseTarget();
                break;

            case SoldierStates.Stunned:
                PlayerDetected(false);
                StunnedSoldier();
                break;
        }
        
    }

    #region Set Soldier at the start
    // Set the initial Soldier'State, since at the start of the level will check if he have an object into the field's inspector.
    // if he has this gameobject will take all his children's Transform and will set the Soldier's State to Patrolling and fills and array.
    // if there isn't set the Soldier's State to Normal.

    private void SetStartingState()
    {
        if(chris_patrolPointsBox != null)
        {
            int numberOfPatrolPoints = chris_patrolPointsBox.transform.childCount;

            MG_PatrolPoints = new Transform[numberOfPatrolPoints];

            for (int i = 0; i < numberOfPatrolPoints; i++)
            {
                MG_PatrolPoints[i] = chris_patrolPointsBox.transform.GetChild(i);
            }
            
        }
        if (MG_PatrolPoints.Length == 0)
        {
            chris_startedState = SoldierStates.Normal;
        }
        else
        {
            chris_startedState = SoldierStates.Patrolling;
        }
    }

    private void SaveStartingVariables()
    {
        chris_animationsSpeed = chris_enemyAnimatorController.speed;
        chris_startedPosition = transform.position;
        chris_startedRotation = transform.rotation;
        chris_startedSpeed = chris_enemyAgent.speed;
        chris_soldierCurrentState = chris_startedState;
    }
    #endregion

    // Change state
    public void ChangeSoldierState(SoldierStates newSoldierState)
    {
        chris_soldierCurrentState = newSoldierState;
    }
    
    // Set the destination of his NavMeshAgent toRight the player position.     
    public void MoveOnTargetDestination(Vector3 destinationPoint)
    {
        chris_enemyAgent.SetDestination(destinationPoint);
    }

    //Set the agent's speed
    private void CheckIfCanMove()
    {
        if (GameManager.MG_Instance.MG_CurrentGameStates == GameStates.Running)
        {
            chris_enemyAgent.isStopped = false;

            if (chris_soldierCurrentState != SoldierStates.Allert)
            {
                chris_enemyAgent.speed = chris_startedSpeed;
            }
            else
            {
                chris_enemyAgent.speed = MG_AllertSpeed;
            }
            if(chris_soldierCurrentState != SoldierStates.LoseTarget)
            {
                chris_numberOfRotation = 0;
            }
        }
        else
        {
            chris_enemyAgent.speed = 0;
            chris_enemyAgent.isStopped = true;
        }
    }


    #region ForPatrol
    //If the Soldier can patrol (see SetStartingState) set his destination to one of the Transform filled at the start.
    //When reach one increase a count by one (this count is bigger than the array's lenght).
    //When the count reach the max value, it will be 0. So the Soldier will set his destination to the count value.
    private void Patrolling()
    {
        if(MG_PatrolPoints.Length != 0)
        {
            MoveOnTargetDestination(chris_patrolPointsBox.transform.GetChild(chris_patrolCount).position);

            if (chris_enemyAgent.remainingDistance - chris_enemyAgent.stoppingDistance <= 0.2f)
            {
                chris_patrolCount++;

                if(chris_patrolCount >= MG_PatrolPoints.Length)
                {
                    chris_patrolCount = 0;
                }
            }
        }
        
    }
    #endregion

    #region Allert Soldier State
    private void Allerted()
    {
        if (MG_PlayerDetected && chris_soldierCurrentState != SoldierStates.Stunned)
        {
            ChangeSoldierState(SoldierStates.Allert);
        }

    }

    //Go to the player positions.
    // if there is bit distance between the soldier and the player the status game will change in Lose.
    private void AllertState()
    {
        if (MG_PlayerDetected)
        {
            MoveOnTargetDestination(MG_PlayerPosition);
            transform.LookAt(new Vector3(MG_PlayerPosition.x, transform.position.y, MG_PlayerPosition.z));

            if(Vector3.Distance(transform.position, MG_PlayerPosition) <= chris_enemyAgent.stoppingDistance + chris_distanceToLoseGame)
            {
                GameManager.MG_Instance.SetManagerState(GameStates.Lose);
            }

        }
        else if (MG_PlayerDetected == false && chris_enemyAgent.velocity.sqrMagnitude == 0f)
        {
            chris_enemyAgent.speed = chris_startedSpeed;
            ChangeSoldierState(SoldierStates.LoseTarget);
        }
    }
    #endregion
    
    #region Lose Target With Rotation
    //If the soldier lose the targer check the area near him.
    //When reach the last rotation reset the soldier and tell him to return to the start position.
    private void LoseTarget()
    {
        
        if (chris_numberOfRotation < chris_desideredRotations)
        {
            EnemyRotation();
        }
        else
        {
            ChangeSoldierState(chris_startedState);
            chris_numberOfRotation = 0;
            chris_returnToPosition = true;
        }
    }

    // Used to rotate the soldier with a speed, a timer and a bool. 
    //When the timer reach the maxTime will change the bool. This bool determines the rotation's direction. 
    void EnemyRotation()
    {
        bool firstChangeDirection = true;

        if(chris_enemyAgent.remainingDistance - chris_enemyAgent.stoppingDistance <= 0.05f)
        {
            if (chris_changeRotationDirection)
        {
            transform.Rotate(new Vector3(0, chris_rotationSpeed, 0));
        }
        else if (!chris_changeRotationDirection)
        {
            transform.Rotate(new Vector3(0, -chris_rotationSpeed, 0));
        }

        if (firstChangeDirection && Timer(chris_DelayChangeRotation))
        {
            chris_changeRotationDirection = !chris_changeRotationDirection;
            firstChangeDirection = false;
            chris_numberOfRotation++;
        }
        if (firstChangeDirection == false && Timer(chris_DelayChangeRotation * 2))
        {
            chris_changeRotationDirection = !chris_changeRotationDirection;
            chris_numberOfRotation++;
        }
        }
    }
    
    private bool Timer(float destinationTime)
    {
        chris_timer += Time.deltaTime;

        if (chris_timer >= destinationTime)
        {
            chris_timer = 0f;
            return true;
        }
        else
        {
            return false;
        }
    }
    #endregion

    //If the soldier's position is different to the start position and he can move to the starting position.
    //When he reach it, he will reset his rotation.
    private void CheckPosition()
    {
        if (chris_returnToPosition)
        {
            MoveOnTargetDestination(chris_startedPosition);

            if (!chris_enemyAgent.pathPending)
            {
                if (chris_enemyAgent.remainingDistance <= chris_enemyAgent.stoppingDistance)
                {
                    if (!chris_enemyAgent.hasPath || chris_enemyAgent.velocity.sqrMagnitude == 0f)
                    {
                        transform.SetPositionAndRotation(chris_startedPosition, chris_startedRotation);
                        chris_returnToPosition = false;
                    }
                }
            }
        }
    }

    #region Soldier Animations

    //Set the soldier's animation.
    private void AnimationsManager()
    {
        SoldierAnimationsSpeed();
        RandomSoldierIdle();

        if (chris_soldierCurrentState == SoldierStates.Allert)
        {
            chris_enemyAnimatorController.SetBool("allerted", true);
        }
        else
        {
            chris_enemyAnimatorController.SetBool("allerted", false);
        }

        if (chris_soldierCurrentState == SoldierStates.LoseTarget)
        {
            chris_enemyAnimatorController.SetBool("aiming", true);
        }
        else
        {
            chris_enemyAnimatorController.SetBool("aiming", false);
        }
    }

    // Converts the soldier's animations speed with the agentSpeed.
    private void SoldierAnimationsSpeed()
    {
        if (chris_enemyAgent.hasPath && chris_soldierCurrentState != SoldierStates.LoseTarget)
        {
            chris_enemyAnimatorController.SetFloat("speed", chris_enemyAgent.speed);
            chris_enemyAnimatorController.speed = chris_enemyAgent.speed / 2;
        }
        else
        {
            chris_enemyAnimatorController.speed = chris_animationsSpeed;
            chris_enemyAnimatorController.SetFloat("speed", 0);
        }
    }

    // When the soldier is in Idle start a timer. When the timer reach the maxValue randomize the next IdleAnimation.
    private void RandomSoldierIdle()
    {
        if (chris_enemyAnimatorController.GetCurrentAnimatorStateInfo(0).IsName("NormalIdle") && chris_soldierCurrentState != SoldierStates.LoseTarget && !chris_enemyAgent.pathPending)
        {
            chris_animationTimer += Time.deltaTime;

            if (chris_animationTimer >= chris_enemyAnimatorController.GetCurrentAnimatorStateInfo(0).length * chris_animationChangeIdleDelay)
            {
                int randomIdle = Random.Range(0, 2);

                if (randomIdle == 0)
                {
                    PlayEnemyAnimationTrigger("Idle00");
                    chris_animationTimer = 0;
                }
                else if (randomIdle == 1)
                {
                    PlayEnemyAnimationTrigger("Idle01");
                    chris_animationTimer = 0;
                }
            }
        }
    }

    // Set the soldier's animation for the stun.
    private void StunnedSoldier()
    {
        chris_stunnedTimer += Time.fixedDeltaTime;

        if (chris_stunnedTimer < chris_enemyAnimatorController.GetCurrentAnimatorStateInfo(0).length)
        {
            chris_enemyAnimatorController.SetBool("stunned", true);
            chris_soldierCurrentState = SoldierStates.Stunned;
        }
        else
        {
            chris_enemyAnimatorController.SetBool("stunned", false);
            chris_stunnedTimer = 0;
            ChangeSoldierState(SoldierStates.LoseTarget);
        }
        
    }

    //Play soldier's animation trigger with his name.
    public void PlayEnemyAnimationTrigger(string AnimationName)
    {
        chris_enemyAnimatorController.SetTrigger(AnimationName);
    }
    #endregion
}

