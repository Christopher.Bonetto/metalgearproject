﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


public class Enemies : MonoBehaviour
{
    public ICanDetect MG_ReferenceToDetectInterface { get; private set; }

    #region Angle and Radius values

    [Range(0f,360f),SerializeField]
    private float MG_EnemyViewAngle;

    public float CurrentAngle
    {
        get
        {
            return Mathf.Clamp(MG_EnemyViewAngle, 0, 360);
        }
        private set
        {
            MG_EnemyViewAngle = value;
        }
    }

    [SerializeField]
    private float MG_EnemyViewRadius;

    public float CurrentRadius
    {
        get
        {
            return Mathf.Clamp(MG_EnemyViewRadius, 0, 20);
        }
        private set
        {
            MG_EnemyViewRadius = value;
        }
    }
    #endregion

    [SerializeField]
    private LayerMask MG_DetectionMask;

    public Collider[] MG_EnemyDetectedColliders { get; private set; } = new Collider[1];
    public int MG_NumberOfColliderDetected { get; private set; }

    public Vector3 MG_PlayerPosition { get; private set; }
    public bool MG_PlayerDetected { get; private set; }


    #region Minimap Variables

    [SerializeField]
    private RectTransform chris_coneVisionImage;
    [SerializeField]
    private Camera chris_minimapCamera;
    [SerializeField]
    private RectTransform chris_minimap;

    #endregion


    
    public virtual void Awake()
    {
        MG_ReferenceToDetectInterface = new DetectionAreaNoBehaviour();
    }
   
    public virtual void Start()
    {
        MG_PlayerDetected = false;
    }

    /*
    //The enemy will check if there is the interested layer into the detection area, if it happen PlayerDetected(bool) will become true.
      Enemy uses a method to show the detection area in game.
    */
    public virtual void Update()
    {
        if(MG_ReferenceToDetectInterface.DetectArea(gameObject.transform, MG_EnemyViewRadius, MG_EnemyViewAngle, MG_EnemyDetectedColliders, MG_NumberOfColliderDetected, MG_DetectionMask) != null)
        {
            PlayerDetected(true);
            MG_PlayerPosition = MG_ReferenceToDetectInterface.DetectArea(gameObject.transform, MG_EnemyViewRadius, MG_EnemyViewAngle, MG_EnemyDetectedColliders, MG_NumberOfColliderDetected, MG_DetectionMask).transform.position;
        }
        else
        {
            PlayerDetected(false);
        }

        MG_ReferenceToDetectInterface.ShowDetect(gameObject.transform, MG_EnemyViewRadius, MG_EnemyViewAngle, chris_coneVisionImage,chris_minimapCamera,chris_minimap);
    }

    public void PlayerDetected(bool newValue)
    {
        MG_PlayerDetected = newValue;
    }




    #region DetectionGizmo
    //Gizmo too see the OverlapSphere

#if UNITY_EDITOR

    public void OnDrawGizmosSelected()
    {
        EditorGizmo(transform);
    }

    public void EditorGizmo(Transform transform)
    {
        ViewGizmo(Color.green, MG_EnemyViewAngle, MG_EnemyViewRadius);
    }

    private void ViewGizmo(Color color, float angle, float radius)
    {
        Color c = color;
        UnityEditor.Handles.color = c;

        Vector3 rotatedForward = Quaternion.Euler(0, -angle * 0.5f, 0) * transform.forward;

        UnityEditor.Handles.DrawSolidArc(transform.position, Vector3.up, rotatedForward, angle, radius);
    }
#endif

    #endregion

}
