﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyCamera : Enemies
{
    [SerializeField]
    private float MG_RadiusToCallingEnemies;

    private float chris_timer;

    [SerializeField]
    private float MG_RotationSpeed;
    [SerializeField]
    private float MG_DelayChangeRotation;
    private bool chris_changeRotationDirection;

    private bool chris_firstChangeDirection;


    public override void Awake()
    {
        base.Awake();
        
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
        chris_changeRotationDirection = false;
        chris_firstChangeDirection = true;
    }

    // Update is called once per frame
    public override void Update()
    {
        base.Update();

        CheckStates();
        
    }

    /* When the game will start, the security camera will continue check if it see the player. 
    //If it see the player will allert all EnemySoldier into his RadiusToCallingEnemies, 
    //if not it continue to use RotationCameraSystem.
    */
    private void CheckStates()
    {
        if (GameManager.MG_Instance.MG_CurrentGameStates == GameStates.Running)
        {
            if (MG_PlayerDetected)
            {
                AllertNearEnemies();
            }
            else
            {
                RotationCameraSystem();
            }

        }
    }

    // Used to rotate the camera with a speed, a timer and a bool. When the timer reach the maxTime will change the bool.
    //This bool determines the rotation's direction. 
    void RotationCameraSystem()
    {
        if(chris_changeRotationDirection)
        {
            transform.Rotate(new Vector3(0,MG_RotationSpeed, 0));
        }
        else if(!chris_changeRotationDirection)
        {
            transform.Rotate(new Vector3(0,-MG_RotationSpeed, 0));
        }

        if (chris_firstChangeDirection && Timer(MG_DelayChangeRotation))
        {
            chris_changeRotationDirection = !chris_changeRotationDirection;
            chris_firstChangeDirection = false;
        }
        if (chris_firstChangeDirection == false && Timer(MG_DelayChangeRotation * 2))
        {
            chris_changeRotationDirection = !chris_changeRotationDirection;
        }

    }

    private bool Timer(float destinationTime)
    {
        chris_timer += Time.deltaTime;
        
        if(chris_timer >= destinationTime)
        {
            chris_timer = 0f;
            
            return true;
        }
        else
        {
            return false;
        }
        
        
    }

    #region AllertNearEnemies with OverlapSphere
    /*summary
     * After detecting the player with Enemies method store the player position
     * Will allert all the near enemies in an area that they will go in the last player position, setting them with another State.
     */
    void AllertNearEnemies()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, MG_RadiusToCallingEnemies); // must be integrate layer 
        for(int i = 0; i < hitColliders.Length; i++)
        {
            if(hitColliders[i].GetComponent<EnemySoldier>())
            {
                Debug.Log(hitColliders[i].name);
                hitColliders[i].GetComponent<EnemySoldier>().PlayerDetected(true);
                hitColliders[i].GetComponent<EnemySoldier>().MoveOnTargetDestination(MG_PlayerPosition);
                hitColliders[i].GetComponent<EnemySoldier>().ChangeSoldierState(SoldierStates.Allert);
                
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, MG_RadiusToCallingEnemies);
    }
    #endregion
        
}
