﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraStates
{
    Follow,
    GoToPoint,
    Zoom
}

public class CameraActions : MonoBehaviour
{
    private bool chris_startTheGame;
    private bool chris_canMove;

    private Transform chris_cameraTarget;
    private bool chris_followTarget;

    [SerializeField]
    private Vector3 chris_offset;
    public float MG_CameraSpeed;

    [SerializeField]
    private Vector3 chris_endGameOffset;

    [SerializeField]
    private GameObject chris_pointsBox;
    public Transform[] MG_DestinationPoints;
    private int chris_pointCount;
        
    private CameraStates chris_currentCameraState;

    private void Start()
    {
        InizializeCamera();
        chris_startTheGame = false;
    }

    private void Update()
    {

    }

    void LateUpdate()
    {
        CameraStatesManager();
        
    }

    private void CameraStatesManager()
    {
        switch (chris_currentCameraState)
        {
            case CameraStates.Follow:
                FollowPlayer();
                GoStartTheGame();
                break;
            case CameraStates.GoToPoint:
                AutoMoveCamera();
                break;
            case CameraStates.Zoom:
                ZoomOnTarget();
                break;
        }
    }

    //Method called to change the camera state.
    public void ChangeCameraState(CameraStates newState)
    {
        chris_currentCameraState = newState;
    }

    //Follow the player with an offset.
    private void FollowPlayer()
    {
        if (chris_canMove)
        {
            Vector3 addOffset = chris_cameraTarget.position + chris_offset;
            Vector3 LerpPosition = Vector3.Lerp(transform.position, addOffset, MG_CameraSpeed * Time.fixedDeltaTime);
            transform.position = LerpPosition;
            
        }
        
    }

    //Set the camera's state at the start. If it contains an gameobject with children, the camera will fill an waypoint's array.
    //If there isn't this gameobject reference the camera will setted directly to the Follow state.
    private void InizializeCamera()
    {
        chris_canMove = false;

        if(chris_cameraTarget == null)
        {
            ChangeFollowTarget(GameManager.MG_Instance.MG_playerReference.transform);
        }
        
        if (chris_pointsBox != null)
        {
            int numberOfPatrolPoints = chris_pointsBox.transform.childCount;

            MG_DestinationPoints = new Transform[numberOfPatrolPoints];

            for (int i = 0; i < numberOfPatrolPoints; i++)
            {
                MG_DestinationPoints[i] = chris_pointsBox.transform.GetChild(i);
            }
            
        }
        if (MG_DestinationPoints.Length == 0)
        {
            chris_currentCameraState = CameraStates.Follow;
        }
        else
        {
            chris_currentCameraState = CameraStates.GoToPoint;
        }

    }

    public void ChangeFollowTarget(Transform newTarget)
    {
        chris_cameraTarget = newTarget;
    }

    public void ChangeCanMove(bool newValue)
    {
        chris_canMove = newValue;
    }

    //This method is called if the camera have waypoints to reach.
    //Go to waypoint index. When the camera reach the waypoint's position increase the index to set another destination to reach.
    //At the last waypoint reached set the camera's state like Follow.
    private void AutoMoveCamera()
    {
        if (chris_canMove && MG_DestinationPoints.Length != 0 && chris_pointCount < MG_DestinationPoints.Length)
        {
            if(Vector3.Distance(transform.position, chris_pointsBox.transform.GetChild(chris_pointCount).position) > 0.2f)
            {
                transform.position = Vector3.MoveTowards(transform.position, chris_pointsBox.transform.GetChild(chris_pointCount).position, MG_CameraSpeed / 10f);
            }
            else if (Vector3.Distance(transform.position, chris_pointsBox.transform.GetChild(chris_pointCount).position) <= 0.2f)
            {
                chris_pointCount++;
            }
            
        }
        else if(chris_canMove)
        {
            chris_currentCameraState = CameraStates.Follow;
        }
    }

    //When the camera reach the player call to the GM that the game can start.
    private void GoStartTheGame()
    {
        if(!chris_startTheGame)
        if (chris_currentCameraState == CameraStates.Follow && Vector3.Distance(transform.position, chris_cameraTarget.position + chris_offset) < 0.1f)
        {
                GameManager.MG_Instance.SetManagerState(GameStates.Running);
                UIManager.MG_Instance.ShowTimer(true);
                chris_startTheGame = true;
        }
    }

    //Zoom on target with an offset.
    private void ZoomOnTarget()
    {
        Vector3 endGamePosition = chris_cameraTarget.position + chris_endGameOffset;
        Vector3 LerpPosition = Vector3.Lerp(transform.position, endGamePosition, MG_CameraSpeed * Time.deltaTime);
        transform.LookAt(chris_cameraTarget);
        transform.position = LerpPosition;
    }
}
