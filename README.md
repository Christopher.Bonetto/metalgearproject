# MetalGearProject

**Intro**
This project is replica of Metal Gear Solid VR Missions using Unity.
I have implemented some levels of the game where you can find some main features.
The player has to reach the final point to end the level but this one is populated by enemies and cameras.

**Enemies**
Managed with a state machine they can have different states such as: alerted, target loss, patrolling and idle.
They control an angular field in front of them and if the character passes in front of them they will start chasing him.
If alerted they will run to the last point where they saw the character, to make it possible for the player to escape.
Finally if they take the character, the player will lose and have a chance to replay the level.

**Cameras**
These control the surrounding angular area and if they find the player they will warn all nearby enemies.
